# from datetime import datetime
from pathlib import Path
# from typing import Union, Optional
# import pandas as pd
import os
# import platform
# import socket
# from pandas import DataFrame
# from tqdm import trange, tqdm
# from joblib import Parallel, delayed
import pickle #import dill as pickle

from CHFConfigurationSensitivityAnalyzer import CHFConfigurationSensitivityAnalyzer

# ---------------------------
# -----------Main------------
# ---------------------------

# inputs:
if input('insert paths now?(y/n): ') is "y":
    folder_path = input("insert score csvs folder path: ")
    a_dest = input("insert tmp folder path: ")
    outputDir = input("insert output folder path: ")
    jobsNum = input("insert number of parallel jobs: ")
else:
    if os.getenv('COMPUTERNAME') == 'RDSRV':
        root = Path("F:")
    else:
        root = Path(r"\\192.168.55.210\f$")
    folder_path = root / Path(r"Or-rnd\AnalyzingConfigurationSensitivityOfCHFStatus\allDB")
    a_dest = root / Path(r"Or-rnd\AnalyzingConfigurationSensitivityOfCHFStatus\tmp")
    outputDir = root / Path(r"Or-rnd\AnalyzingConfigurationSensitivityOfCHFStatus\outputs")
# checking for available data:
midCalcDataFilePath = a_dest / Path(r"\allDB_23082020.pickle")
if os.path.isfile(str(midCalcDataFilePath)):
    with open(str(midCalcDataFilePath), 'rb') as f:
        a = pickle.load(f)
else:
    a = CHFConfigurationSensitivityAnalyzer(str(folder_path), jobsNum=100)
    with open(str(midCalcDataFilePath), 'wb') as f:
        pickle.dump(a, f)
# calculating csvs:
Tool1_df = a.get_allConfigurations_score_statistics_allPatientsNsentences()
Tool1_df.to_csv(str(outputDir / Path(r"\Tool1_allData_23082020.csv")), index = False)
Tool2_df = a.get_allPatints_score_statistics_givenConfigurations()
Tool2_df.to_csv(str(outputDir / Path(r"\Tool2_allData_23082020.csv")), index = False)
print("done")