from datetime import datetime
from pathlib import Path
from typing import Union, Optional
import pandas as pd


class patientNsentence_CHFConfigurationSensitivityAnalyzer:

    def __init__(self, file_path: Union[Path, str]):

        # Verify wav_path type
        if isinstance(file_path, str):
            file_path = Path(file_path)
            spited_file_path = file_path.name.split("_")

        # class properties:
        self.patient = spited_file_path[10]
        self.HMO = self.patient[0:3]
        self.sentence = spited_file_path[11]
        self.datetime = datetime.strptime(spited_file_path[13]+" "+spited_file_path[14][:-4], "%y%m%d %H%M%S")
        self.columnsNames = ["score", "tw", "gap", "dist", "graph", "phn", "patient", "sentence"]
        self.__clinical_status_correlation_scores_gap_raw_df = None
        self.clinical_status_correlation_scores_gap_df = None

        # load csv:
        self.__clinical_status_correlation_scores_gap_raw_df = pd.read_csv(str(file_path))

        # creating clinical_status_correlation_scores_gap_df:
        self.clinical_status_correlation_scores_gap_df = pd.DataFrame(columns=self.columnsNames)
        for name, score in zip(self.__clinical_status_correlation_scores_gap_raw_df.iloc[:,0], self.__clinical_status_correlation_scores_gap_raw_df.iloc[:,1]):
            self.clinical_status_correlation_scores_gap_df = self.clinical_status_correlation_scores_gap_df.append(self.__devideScoreNameIntoColumns__(name, score), ignore_index=True)
        self.clinical_status_correlation_scores_gap_df = self.clinical_status_correlation_scores_gap_df.reset_index(drop=True)

    def __devideScoreNameIntoColumns__(self, name, score):
        """returns dictionary represent a row in clinical_status_correlation_scores_gap_df"""
        splitedName = name.split("_")
        dist = splitedName[splitedName.index("dist")+1:splitedName.index("graph")]
        row = {"score": score,
               "tw": int(splitedName[1]),
               "gap": int(splitedName[3]),
               "dist": '_'.join(splitedName[splitedName.index("dist")+1:splitedName.index("graph")]),
               "graph": '_'.join(splitedName[splitedName.index("graph")+1:splitedName.index("phn")]),
               "phn": int(splitedName[-1]),
               "patient": self.patient,
               "sentence": self.sentence}
        return row