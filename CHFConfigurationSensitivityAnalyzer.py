from pathlib import Path
from typing import Union
import pandas as pd
import os
from tqdm import tqdm
from joblib import Parallel, delayed

from patientNsentence_CHFConfigurationSensitivityAnalyzer import patientNsentence_CHFConfigurationSensitivityAnalyzer

class CHFConfigurationSensitivityAnalyzer:
    def __init__(self, folder_path: Union[Path, str, list], jobsNum=1):
        # class properties:
        self.allPatientsNsentences_score_statistics_columnsNames = ["tw", "gap", "dist", "graph", "phn", "patientNum", "scoreAverage", "scoreSTD"]
        self.jobsNum = jobsNum
        self.folder_path = None
        self.all_patients_paths = None
        self.unique_patients_list = None
        self.unique_sentence_list = None
        self.__non_unique_noPatient_configurations_list = None
        self.__non_unique_withPatient_configurations_list = None
        self.unique_noPatient_configurations_list = None
        self.unique_withPatient_configurations_list = None
        self.clinical_status_correlation_scores_gap_df = None
        self.__patientNsentence_list = None

        if isinstance(folder_path, str):
            self.folder_path = Path(folder_path)
            self.all_patients_paths = [os.path.join(folder_path, path) for path in os.listdir(folder_path)]
        if isinstance(folder_path, Path):
            self.folder_path = folder_path
            self.all_patients_paths = [os.path.join(folder_path, path) for path in os.listdir(folder_path)]
        if isinstance(folder_path, list):
            self.all_patients_paths = folder_path

        # creating patient data:
        self.patients_list = Parallel(n_jobs=self.jobsNum)(delayed(patientNsentence_CHFConfigurationSensitivityAnalyzer)(self.all_patients_paths[i])
                                                           for i in tqdm(range(len(self.all_patients_paths)), desc='building object...'))
        # create all patient and sentence data score in one df:
        self.clinical_status_correlation_scores_gap_df = pd.DataFrame(columns=self.patients_list[0].columnsNames)
        for patientNsentence in self.patients_list:
            self.clinical_status_correlation_scores_gap_df = self.clinical_status_correlation_scores_gap_df.append(patientNsentence.clinical_status_correlation_scores_gap_df)

        # add configuration string column:
        self.__non_unique_noPatient_configurations_list = [str(tw) +"__"+ str(gap) +"__"+ dist +"__"+ graph +"__"+ str(phn)
                                                 for tw, gap, dist, graph, phn in zip(self.clinical_status_correlation_scores_gap_df["tw"],
                                                                                        self.clinical_status_correlation_scores_gap_df["gap"],
                                                                                        self.clinical_status_correlation_scores_gap_df["dist"],
                                                                                        self.clinical_status_correlation_scores_gap_df["graph"],
                                                                                        self.clinical_status_correlation_scores_gap_df["phn"])]
        self.__non_unique_withPatient_configurations_list = [patient + "__" + str(tw) + "__" + str(gap) + "__" + dist + "__" + graph + "__" + str(phn)
                                                for patient, tw, gap, dist, graph, phn in zip(self.clinical_status_correlation_scores_gap_df["patient"],
                                                                                              self.clinical_status_correlation_scores_gap_df["tw"],
                                                                                             self.clinical_status_correlation_scores_gap_df["gap"],
                                                                                             self.clinical_status_correlation_scores_gap_df["dist"],
                                                                                             self.clinical_status_correlation_scores_gap_df["graph"],
                                                                                             self.clinical_status_correlation_scores_gap_df["phn"])]

                # initialize static class properties:
        self.unique_noPatient_configurations_list = list(set(self.__non_unique_noPatient_configurations_list))
        self.unique_withPatient_configurations_list = list(set(self.__non_unique_withPatient_configurations_list))
        self.unique_patients_list = set(self.clinical_status_correlation_scores_gap_df["patient"])
        self.unique_sentence_list = set(self.clinical_status_correlation_scores_gap_df["sentence"])

    # methods:
    # -------

    def get_configuration_score_statistics(self, patient=None, tw=None, gap=None, dist=None, graph=None, phn=None):
        """
        calculate score average and STD for all scores given a configuration.
        data is filtered by given configuration values, if one or more configuration values is not given it will not
        be a part of the configuration and the data will not be filtered by it.

        :param patient: str patient id
        :param tw: number-int or str
        :param gap: number-int or str
        :param dist: str
        :param graph: str
        :param phn: number-int or str
        :return: dictionary
        """
        filtered_df: pd.DataFrame = self.clinical_status_correlation_scores_gap_df.copy(deep=True)
        filtered_df = filtered_df.reset_index(drop=True)
        patient_config_num = "patientNum"
        # filtering:
        if patient is not None: patient_config_num="sub-configurationsNum"; filtered_df = filtered_df[filtered_df["patient"] == patient]
        if tw is not None: tw=int(tw); filtered_df = filtered_df[filtered_df["tw"]==int(tw)]
        if gap is not None: gap=int(gap); filtered_df = filtered_df[filtered_df["gap"]==int(gap)]
        if dist is not None: filtered_df = filtered_df[filtered_df["dist"]==dist]
        if graph is not None: filtered_df = filtered_df[filtered_df["graph"]==graph]
        if phn is not None: phn=int(phn); filtered_df = filtered_df[filtered_df["phn"]==int(phn)]

        if len(filtered_df) == 0: return []

        # creat row:
        row = {"tw": tw,
               "gap": gap,
               "dist": dist,
               "graph": graph,
               "phn": phn,
               patient_config_num: len(filtered_df) if patient is not None else len(set(filtered_df["patient"])),
               "scoreAverage": filtered_df["score"].mean(axis=0),
               "scoreSTD": filtered_df["score"].std(axis=0)} #nan meaning no value or 1 value
        if patient is not None: row["patient"] = patient

        return row

    def get_allConfigurations_score_statistics_allPatientsNsentences(self):
        """statistics for all available configuration of tw, gap, dist, graph, phn"""

        tmpDictList = Parallel(n_jobs=self.jobsNum)(delayed(self.get_configuration_score_statistics)
                                                    (tw=self.unique_noPatient_configurations_list[i].split("__")[0],
                                                     gap=self.unique_noPatient_configurations_list[i].split("__")[1],
                                                     dist=self.unique_noPatient_configurations_list[i].split("__")[2],
                                                     graph=self.unique_noPatient_configurations_list[i].split("__")[3],
                                                     phn=self.unique_noPatient_configurations_list[i].split("__")[4])
                                              for i in tqdm(range(len(self.unique_noPatient_configurations_list)), desc='calculating allConfigurations score statistics for all patients and sentences...'))
        # make df from dicts:
        # for i in tqdm(range(len(tmpDictList))):
        #     if tmpDictList[i] != []:
        #         allConfigurations_score_statistics_allPatientsNsentences_df = allConfigurations_score_statistics_allPatientsNsentences_df.append(tmpDictList[i], ignore_index=True)
        allConfigurations_score_statistics_allPatientsNsentences_df = pd.DataFrame.from_dict(tmpDictList)
        allConfigurations_score_statistics_allPatientsNsentences_df: pd.DataFrame = allConfigurations_score_statistics_allPatientsNsentences_df.reset_index(drop=True)

        return allConfigurations_score_statistics_allPatientsNsentences_df

    def get_allPatints_score_statistics_givenConfigurations(self):
        """statistics for all available configuration for each unique property list (tw, gap, dist, graph, phn) for each patient"""
        tmpDictList = []
        patient_list = list(set(self.clinical_status_correlation_scores_gap_df["patient"]))
        for i in tqdm(range(len(patient_list))):
            patient = patient_list[i]
            for subConfig in ["tw", "gap", "dist", "graph", "phn"]:
                unique_subConfigs = list(set(self.clinical_status_correlation_scores_gap_df[subConfig]))
                for unique_subConfig in unique_subConfigs:
                    tmpDictList.append(self.get_configuration_score_statistics(patient=patient, **{subConfig: unique_subConfig}))
        tmpDictList = [x for x in tmpDictList if x != []] # clean list
        allConfigurations_score_statistics_allPatientsNsentences_df = pd.DataFrame.from_dict(tmpDictList)
        allConfigurations_score_statistics_allPatientsNsentences_df: pd.DataFrame = allConfigurations_score_statistics_allPatientsNsentences_df.reset_index(
            drop=True)

        return allConfigurations_score_statistics_allPatientsNsentences_df
